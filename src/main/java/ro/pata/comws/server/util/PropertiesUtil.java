package ro.pata.comws.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Returns values from an external property file.
 */
public class PropertiesUtil extends PropertyPlaceholderConfigurer {
    private static final Logger log = LoggerFactory.getLogger(PropertiesUtil.class);

    private Map<String, String> propertiesMap = new HashMap<>();

    public PropertiesUtil(String file) throws IOException {
        loadProperties(Paths.get(file));
    }

    public void loadProperties(Path path) throws IOException {
        Properties props = new Properties();
        try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ)) {
            props.load(is);
            for (Object okey: props.keySet()) {
                propertiesMap.put((String)okey, props.get((String)okey).toString());
            }
        }
    }

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) {
        super.processProperties(beanFactory, props);

        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String value = props.getProperty(keyStr);
            propertiesMap.put(keyStr, value);
        }
    }

    public String getProperty(String name) {
        String value = propertiesMap.get(name);
        if (value == null)
            log.warn("Configuration item '{}' requested but not found in configuration!",name);
        return value;
    }

    public String getProperty(String name, String defaultValue) {
        String value = propertiesMap.get(name);
        return value == null ? defaultValue : value;
    }

    public int getPropertyInt(String name) {
        if (!propertiesMap.containsKey(name))
            throw new PropertyException("Property "+name+" not found");
        return Integer.valueOf(propertiesMap.get(name).trim());
    }

    public int getPropertyInt(String name, int defaultValue) {
        String val = propertiesMap.get(name);
        if (val == null) return defaultValue;
        return Integer.valueOf(val.trim());
    }
}

class PropertyException extends RuntimeException{
    public PropertyException(String msg){
        super(msg);
    }
}
