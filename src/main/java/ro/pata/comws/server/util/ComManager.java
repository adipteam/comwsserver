package ro.pata.comws.server.util;

import com.fazecast.jSerialComm.SerialPort;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manage the connected COM ports.
 */
public class ComManager {
    Integer connectedPortId=0; //Value is incremented on each new connection.
    Map<Integer,ComLocal> connectedPorts=new HashMap<>(); //Store for the connected ports with their IDs.

    /**
     * Get system port names for all available local COM ports.
     * @return
     */
    public List<String> getPortNames(){
        List<String> portNames=new ArrayList<>();
        SerialPort[] localPorts=SerialPort.getCommPorts();

        for(SerialPort localPort:localPorts) portNames.add(localPort.getSystemPortName());

        return portNames;
    }

    /**
     * Create a new connection to a local COM port.
     * @param portName
     * @return the ID of the connection or -1 if the connection could not be opened.
     */
    public int connectTo(String portName){
        SerialPort[] localPorts=SerialPort.getCommPorts();

        for(SerialPort localPort:localPorts){
            if(localPort.getSystemPortName().equals(portName)){
                ComLocal coml=new ComLocal(localPort);
                if(!connectedPorts.containsValue(coml) && coml.start()) {
                    connectedPorts.put(++connectedPortId, coml);
                    return connectedPortId;
                }
            }
        }
        return -1;
    }

    /**
     * Write data to a connected COM port.
     * @param port
     * @param data
     */
    public void sendData(int port, byte[] data){
        connectedPorts.get(port).sendData(data);
    }

    /**
     * Disconnect all connected local COM ports.
     */
    public void disconnectAll(){
        for(ComLocal p:connectedPorts.values()) p.closePort();
        connectedPorts=new HashMap<>();
    }

    /**
     * Get data received from a local COM port.
     * @param portId
     * @return
     */
    public byte[] getData(int portId){
        if(connectedPorts.get(portId).hasReceivedData()) {
            return connectedPorts.get(portId).getReceivedData();
        } else {
            return new byte[0];
        }
    }
}
