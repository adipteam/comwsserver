package ro.pata.comws.server.util;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import java.util.*;

/**
 * Class that manages the connection to a specific COM port.
 * It is created when the local port is opened and destroyed when the connection is closed.
 */
public class ComLocal {
    private SerialPort port; //Local COM port
    private Deque<byte[]> receivedData=new ArrayDeque<>(); //A buffer for received data

    public ComLocal(SerialPort port) {
        this.port = port;
    }

    /**
     * Tries to open the COM port, sets baud and adds a listener for receiving data.
     * @return true if the port was successfully opened.
     */
    public boolean start(){
        port.setBaudRate(9600);
        if(!port.openPort()) return false;
        port.addDataListener(new SerialPortDataListenerImpl());
        return true;
    }

    public void sendData(byte[] data){
        port.writeBytes(data,data.length);
    }

    public void closePort(){
        port.closePort();
    }

    /**
     * Return true if there is data in the receive buffer.
     * @return
     */
    public boolean hasReceivedData(){
        return !receivedData.isEmpty();
    }

    /**
     * Transforms the received buffer into a byte array and return it.
     * @return
     */
    public byte[] getReceivedData(){
        List<Byte> recb=new ArrayList<>();
        while(!receivedData.isEmpty()){
            byte[] data=receivedData.removeLast();
            for(byte b:data) recb.add(b);
        }

        byte[] data=new byte[recb.size()];
        for(int i=0;i<recb.size();i++) data[i]=recb.get(i);

        return data;
    }

    private class SerialPortDataListenerImpl implements SerialPortDataListener {

        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
        }

        @Override
        public void serialEvent(SerialPortEvent event) {
            if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                return;
            byte[] newData = new byte[port.bytesAvailable()];
            port.readBytes(newData, newData.length);
            receivedData.addFirst(newData);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComLocal comLocal = (ComLocal) o;
        return port.getSystemPortName().equals(comLocal.port.getSystemPortName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(port.getSystemPortName());
    }
}
