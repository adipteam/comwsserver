package ro.pata.comws.server.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * This webservice interface exposes remote functionality to interact with COM ports.
 */

@WebService
public interface ComPortWebservice {
    /**
     * Get data from a connected port.
     * @param port is the connection id for a connected COM port.
     * @return data received on the specified port.
     */
    @WebMethod
    byte[] getReceivedData(int port);

    /**
     * Write data to a connected COM port.
     * @param port is the connection id for a connected COM port.
     * @param data to be written.
     */
    @WebMethod
    void sendData(int port, byte[] data);

    /**
     * Return all COM port names available on the system.
     * @return a list of getSystemPortName().
     */
    @WebMethod
    List<String> getServicePortNames();

    /**
     * Connect to a COM port.
     * @param portName the getSystemPortName() to connect to.
     * @return
     */
    @WebMethod
    int connectTo(String portName);

    /**
     * Disconnect all connected ports.
     */
    @WebMethod
    void disconnectAll();
}
