package ro.pata.comws.server.ws;

import org.springframework.beans.factory.annotation.Autowired;
import ro.pata.comws.server.util.ComManager;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ro.pata.comws.server.ws.ComPortWebservice")
public class ComPortWebserviceImpl implements ComPortWebservice {

    /**
     * The commands received by the webservice interface will be forwarded to the COM port manager.
     */
    @Autowired
    ComManager comPortManager;

    public byte[] getReceivedData(int portId) {
        return comPortManager.getData(portId);
    }

    public void sendData(int portId, byte[] data) {comPortManager.sendData(portId,data);}

    @Override
    public List<String> getServicePortNames() {
        return comPortManager.getPortNames();
    }

    @Override
    public int connectTo(String portName) {
        return comPortManager.connectTo(portName);
    }

    @Override
    public void disconnectAll() {
        comPortManager.disconnectAll();
    }
}
