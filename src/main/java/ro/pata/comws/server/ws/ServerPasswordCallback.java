package ro.pata.comws.server.ws;

import org.apache.wss4j.common.ext.WSPasswordCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Server-side password callback
 *
 * User: magnusmickelsson
 * Date: 2010-aug-27
 * Time: 09:58:12
 * Web: https://www.citerus.se/securing-webservices-without-going-crazy/
 */
public class ServerPasswordCallback implements CallbackHandler
{
    private Map<String, String> passwords = new HashMap<String, String>();

    public ServerPasswordCallback()
    {
        passwords.put("service", "salam");
    }

    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
    {
        for (Callback callback : callbacks)
        {
            WSPasswordCallback passwordCallback = (WSPasswordCallback) callback;

            String password = passwords.get(passwordCallback.getIdentifier());
            if (password != null)
            {
                passwordCallback.setPassword(password);
                return;
            }
        }
    }

}