package ro.pata.comws.server;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.XmlWebApplicationContext;
import ro.pata.comws.server.util.PropertiesUtil;


/**
 * Start Jetty server and load the defined web services.
 */
public class EmbeddedWebServer implements ApplicationContextAware {
    private static final String HTTP_PORT = "http.port"; //listen port for Jetty
    private static final String HTTPS_PORT = "https.port"; //listen port for Jetty
    private static final String HTTP_CONTEXT = "http.context"; //context for the webservice servlet

    @Autowired
    private PropertiesUtil props;

    private Server server;

    private ApplicationContext appContext;
    private XmlWebApplicationContext webContext;

    @PostConstruct
    public void start() throws Exception {

        server = new Server();

        //HTTP port config
        try (ServerConnector httpCon = new ServerConnector(server)) {
            httpCon.setPort(props.getPropertyInt(HTTP_PORT));
            server.addConnector(httpCon);
        }

        //HTTPS port config
        /*
        HttpConfiguration https = new HttpConfiguration();
        https.addCustomizer(new SecureRequestCustomizer());
        SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setKeyStorePath("cert.ks");
        sslContextFactory.setKeyStorePassword("salam");
        sslContextFactory.setKeyManagerPassword("salam");
        sslContextFactory.setCertAlias("test01");
        try (ServerConnector httpsCon = new ServerConnector(server,
                new SslConnectionFactory(sslContextFactory, "http/1.1"),
                new HttpConnectionFactory(https))) {
            httpsCon.setPort(props.getPropertyInt(HTTPS_PORT));

            server.addConnector(httpsCon);
        }
        */


        final ServletHolder servletHolder = new ServletHolder( new CXFServlet() );
        final ServletContextHandler context = new ServletContextHandler();

        context.setContextPath(props.getProperty(HTTP_CONTEXT));
        context.addServlet(servletHolder, "/*");

        webContext = new XmlWebApplicationContext();
        webContext.setParent(appContext);
        webContext.setServletContext(context.getServletContext());
        webContext.setConfigLocations("classpath:/web-beans.xml");
        webContext.refresh();

        context.addEventListener(new ContextLoaderListener(webContext));

        HandlerCollection handlers = new HandlerCollection();
        handlers.addHandler(context);

        // add request log
        //RequestLogHandler requestLogHandler = new RequestLogHandler();
        //requestLogHandler.setRequestLog(new Slf4jRequestLog());
        //handlers.addHandler(requestLogHandler);

        server.setHandler(handlers);
        server.start();
    }

    @PreDestroy
    public void stop() throws Exception {
        server.stop();
        webContext.close();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        appContext = applicationContext;
    }
}
