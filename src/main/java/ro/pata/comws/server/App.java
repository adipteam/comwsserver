package ro.pata.comws.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * This application exposes a webservice which allows remote connection to a COM port.
 */
public class App {
    static {
        if (!System.getProperties().containsKey("logback.configurationFile")) System.setProperty("logback.configurationFile", "config/logback.xml");
    }
    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args)  {
        log.info("Starting server");
        App app=new App();
        app.run();
    }

    private void run()  {
        GenericXmlApplicationContext ctx;
        ctx = new GenericXmlApplicationContext();
        ctx.registerShutdownHook();
        ctx.load("classpath:Beans.xml");
        ctx.refresh();
    }
}
